#!/bin/sh

#   extractfidparams - get dwell time (epi) and asym time (field map) 
#                      from fid dirs
#
#   Stephen Smith and Mark Jenkinson, FMRIB Image Analysis Group
#
#   Copyright (C) 1999-2004 University of Oxford
#
#   SHCOPYRIGHT

if [ $# -lt 1 -o X$1 = X-h -o X$1 = X-help -o X$1 = X--help ] ; then
  echo "Script for extracting sequence parameters from Varian fid directories"
  echo "It will give the dwell time (in seconds) for an epi acquisition"
  echo "It will give the asym time (in seconds) for a fieldmap acquisition"
  echo " "
  echo "Usage: `basename $0` fid_directory"
  exit -1
fi

epi=$1;
if [ $# -eq 2 ] ; then
 fmap=$2;
else
 fmap=$1;
fi

# for a fieldmap
asym=`extracttxt '^asym[^a-z0-9]' $fmap/procpar 1 1 | cut -d' ' -f3`
if [ X${asym}X != XX ] ; then
 if [ $# -lt 2 ] ; then 
   echo $asym;
   exit 0; 
 fi
fi

# otherwise it is a normal epi
np=`extracttxt '^np[^a-z0-9]' $epi/procpar 1 1 | cut -d' ' -f2`
sw=`extracttxt '^sw[^a-z0-9]' $epi/procpar 1 1 | cut -d' ' -f2`
lro=`extracttxt '^lro[^a-z0-9]' $epi/procpar 1 1 | cut -d' ' -f2`
gmax=`extracttxt '^gmax[^a-z0-9]' $epi/procpar 1 1 | cut -d' ' -f2`
echo_del=`extracttxt '^echo_del[^a-z0-9]' $epi/procpar 1 1 | cut -d' ' -f2`
num_ints=`extracttxt '^num_ints[^a-z0-9]' $epi/procpar 1 1 | cut -d' ' -f2`
grise=`extracttxt '^grise_copy[^a-z0-9]' $epi/procpar 1 1 | cut -d' ' -f2`
if [ X${grise}X = XX ] ; then grise=0; fi
if [ $grise = 0 ] ; then grise=0.0002; fi

# echo $np $sw $lro $gmax $echo_del $num_ints $grise

tau_pe=`echo "12 k $np 2.0 / $sw / 2.0 $sw * $grise * $lro / 4257.5 / $gmax / $echo_del 1000000 / + + $num_ints / p" | dc`;

if [ $# -lt 2 ] ; then 
  echo $tau_pe;
else
  echo "12 k $tau_pe $asym / p" | dc ;
fi

exit 0





